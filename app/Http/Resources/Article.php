<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Article extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */

    use HasFactory;

    protected $fillable = [
      'title', 
      'body', 
      'autor'
    ];

    public function autor() {
      return $this->belongsTo(\App\Models\User::class, 'autor', 'id');
    }

    public function toArray($request)
    {
        return parent::toArray($request);
    }
}
