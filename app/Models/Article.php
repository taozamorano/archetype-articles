<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    use HasFactory;
    protected $fillable = [
      'title',
      'body', 
      'autor'
    ];

    function autor() {
      return $this->belongsTo(\App\Models\User::class, 'autor', 'id');
    }
}
